#!/bin/bash

# Check if minimum 3 arguments are provided (recipient, subject, at least one file)
if [ "$#" -lt 3 ]; then
	    echo "Usage: $0 <recipient_email> <subject> <file1> [file2] ... [fileN]"
	        exit 1
fi

recipient="$1"
subject="$2"
shift 2  # Remove the first two arguments

boundary="ZZ_/afg6432dfgkl.94531q"

# Construct the email header
email_header="From: sender@example.com
To: $recipient
Subject: $subject
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary=\"$boundary\"
--$boundary
Content-Type: text/plain

"

# Generate email body with filenames and wc output
email_body=""
for file in "$@"; do
	    if [ -f "$file" ]; then
		            email_body+="$file\n"
			            email_body+="$(wc "$file")\n\n"
				        else
						        echo "File $file not found. Skipping."
							    fi
						    done

						    # Attach files
						    attachments=""
						    for file in "$@"; do
							        if [ -f "$file" ]; then
									        attachments+="--$boundary\n"
										        attachments+="Content-Type: $(file -b --mime-type "$file"); name=\"$file\"\n"
											        attachments+="Content-Transfer-Encoding: base64\n"
												        attachments+="Content-Disposition: attachment; filename=\"$file\"\n\n"
													        attachments+="$(base64 "$file")\n"
														    fi
													    done

													    # Combine everything and send the email
													    full_email="$email_header$email_body$attachments--$boundary--"
													    echo -e "$full_email" | /usr/sbin/sendmail -t

													    echo "Email sent to $recipient!"

